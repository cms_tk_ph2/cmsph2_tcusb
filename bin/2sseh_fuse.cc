#include "USB_libusb.h"
#include <chrono>
#include <cstring>
#include <fstream>
#include <iostream>
#include <thread>
#include <vector>

std::vector<std::pair<int, int>> read_lpgbt_min_config(std::string file)
{
    std::ifstream                    myReadFile(file);
    std::vector<std::pair<int, int>> myreg;

    std::string line;
    while(std::getline(myReadFile, line))
    {
        std::stringstream   ss(line);
        std::pair<int, int> reg;
        if(ss >> reg.first >> std::hex >> reg.second >> std::dec)
        {
            std::cout << reg.first << " " << std::hex << +reg.second << std::dec << std::endl;
            myreg.push_back(reg);
        }
    }
    return myreg;
}

void configureI2C(TC_2SSEH* pTC_2SSEH)
{
    pTC_2SSEH->write_i2c(0x020, 0xf8);
    pTC_2SSEH->write_i2c(0x021, 0x24);
    pTC_2SSEH->write_i2c(0x022, 0x44);
    pTC_2SSEH->write_i2c(0x023, 0x55);

    pTC_2SSEH->write_i2c(0x024, 0x55);
    pTC_2SSEH->write_i2c(0x025, 0x55);
    pTC_2SSEH->write_i2c(0x026, 0x55);
    pTC_2SSEH->write_i2c(0x027, 0x55);

    pTC_2SSEH->write_i2c(0x028, 0x0f);
    pTC_2SSEH->write_i2c(0x029, 0x36);
    pTC_2SSEH->write_i2c(0x02a, 0x00);
    pTC_2SSEH->write_i2c(0x02b, 0x00);

    pTC_2SSEH->write_i2c(0x02c, 0xa0);
    pTC_2SSEH->write_i2c(0x02d, 0x89);
    pTC_2SSEH->write_i2c(0x02e, 0x99);
    pTC_2SSEH->write_i2c(0x02f, 0x0a);

    pTC_2SSEH->write_i2c(0x030, 0x1a);
    pTC_2SSEH->write_i2c(0x031, 0x2a);
    pTC_2SSEH->write_i2c(0x032, 0x3a);
    pTC_2SSEH->write_i2c(0x033, 0x0a);

    pTC_2SSEH->write_i2c(0x034, 0x64);
    pTC_2SSEH->write_i2c(0x035, 0x00);
    pTC_2SSEH->write_i2c(0x036, 0x80);
    pTC_2SSEH->write_i2c(0x037, 0x18);

    pTC_2SSEH->write_i2c(0x038, 0x00);
    pTC_2SSEH->write_i2c(0x039, 0x40);
    pTC_2SSEH->write_i2c(0x03a, 0x20);
    pTC_2SSEH->write_i2c(0x03b, 0x03);

    pTC_2SSEH->write_and_check_i2c(0x0ef, 0x06);
    pTC_2SSEH->read_i2c(0x1c7);
}

void switchOn(TC_2SSEH* pTC_2SSEH)
{
    float k;
    pTC_2SSEH->set_SehSupply(pTC_2SSEH->sehSupply_On);
    std::this_thread::sleep_for(std::chrono::milliseconds(1200));
    pTC_2SSEH->read_supply(pTC_2SSEH->I_SEH, k);

    pTC_2SSEH->read_load(pTC_2SSEH->U_P1V2_R, k);
    pTC_2SSEH->read_load(pTC_2SSEH->I_P1V2_R, k);
    pTC_2SSEH->read_load(pTC_2SSEH->U_P1V2_L, k);
    pTC_2SSEH->read_load(pTC_2SSEH->I_P1V2_L, k);
}

int main()
{
    int           m = 0;
    bool          wahr;
    unsigned char l;
    unsigned int  test = 0xabcd;

    // cTC_2SSEH.fuse(0x020,0xf8,0x24,0x44,0x55);
    // cTC_2SSEH.fuse(0x024,0x55,0x55,0x55,0x55);
    // cTC_2SSEH.fuse(0x028,0x0f,0x36,0x00,0x00);
    // cTC_2SSEH.fuse(0x02c,0xa0,0x89,0x99,0x0a);
    // cTC_2SSEH.fuse(0x030,0x1a,0x2a,0x3a,0x0a);
    // cTC_2SSEH.fuse(0x034,0x64,0x00,0x80,0x18);
    // cTC_2SSEH.fuse(0x038,0x00,0x40,0x20,0x03);

    // cTC_2SSEH.fuse(0x0ec,0x00,0x00,0x00,0x07);

    TC_2SSEH cTC_2SSEH;

    /* switchOn(&cTC_2SSEH);
    configureI2C(&cTC_2SSEH);

    cTC_2SSEH.fuse(0x020,0xf8,0x24,0x44,0x55);
    std::this_thread::sleep_for (std::chrono::milliseconds (1500) );

    cTC_2SSEH.fuse(0x024,0x55,0x55,0x55,0x55);
    std::this_thread::sleep_for (std::chrono::milliseconds (1500) );

    cTC_2SSEH.fuse(0x028,0x0f,0x36,0x00,0x00);
    std::this_thread::sleep_for (std::chrono::milliseconds (1500) );

    cTC_2SSEH.fuse(0x02c,0xa0,0x89,0x99,0x0a);
    std::this_thread::sleep_for (std::chrono::milliseconds (1500) );

    cTC_2SSEH.fuse(0x030,0x1a,0x2a,0x3a,0x0a);
    std::this_thread::sleep_for (std::chrono::milliseconds (1500) );

    cTC_2SSEH.fuse(0x034,0x64,0x00,0x80,0x18);
    std::this_thread::sleep_for (std::chrono::milliseconds (1500) );

    cTC_2SSEH.fuse(0x038,0x00,0x40,0x20,0x03);
    std::this_thread::sleep_for (std::chrono::milliseconds (1500) );

    cTC_2SSEH.set_SehSupply(cTC_2SSEH.sehSupply_Off);
    std::this_thread::sleep_for (std::chrono::milliseconds (1500) );*/

    switchOn(&cTC_2SSEH);
    configureI2C(&cTC_2SSEH);

    std::this_thread::sleep_for(std::chrono::milliseconds(1200));

    cTC_2SSEH.read_efuses(0x020);
    cTC_2SSEH.read_efuses(0x024);
    cTC_2SSEH.read_efuses(0x028);
    cTC_2SSEH.read_efuses(0x02c);
    cTC_2SSEH.read_efuses(0x030);
    cTC_2SSEH.read_efuses(0x034);
    cTC_2SSEH.read_efuses(0x038);

    /* switchOn(&cTC_2SSEH);
    configureI2C(&cTC_2SSEH);
    cTC_2SSEH.fuse(0x0ec,0x00,0x00,0x00,0x07); */

    std::this_thread::sleep_for(std::chrono::milliseconds(1200));
    cTC_2SSEH.set_SehSupply(cTC_2SSEH.sehSupply_Off);

    return 0;
}
