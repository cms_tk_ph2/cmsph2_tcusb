#include "USB_libusb.h"
#include <chrono>
#include <cstring>
#include <fstream>
#include <iostream>
#include <thread>
#include <vector>

std::vector<std::pair<int, int>> read_lpgbt_min_config(std::string file)
{
    std::ifstream                    myReadFile(file);
    std::vector<std::pair<int, int>> myreg;

    std::string line;
    while(std::getline(myReadFile, line))
    {
        std::stringstream   ss(line);
        std::pair<int, int> reg;
        if(ss >> reg.first >> std::hex >> reg.second >> std::dec)
        {
            std::cout << reg.first << " " << std::hex << +reg.second << std::dec << std::endl;
            myreg.push_back(reg);
        }
    }
    return myreg;
}

int main()
{
    // auto cRegs = read_lpgbt_min_config("lpgbt_minimal_config.txt");
    // unsigned char reg_value_buf[333]={0x00,0x00,0x02,0x00,
    //                                     0x41,0x01,0x00,0x00,
    //                                     0x40,0x40,0x01,0x41};
    // for(unsigned short int i = 0; i < 317; i++){
    // reg_value_buf[i+12]=cRegs[i].second;
    // }
    // for (int reg_value : reg_value_buf)
    // {
    //     std::cout << reg_value <<  std::endl;
    // }
    float         k;
    int           m = 0;
    bool          wahr;
    unsigned char l;
    unsigned int  test = 0xabcd;
    // unsigned char read_input_buf[]={0x12,0x34,0xab,0xcd,0x00,0x00,0x00,0x00,
    // 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
    // 0x00,0x00,(unsigned char)(test>>8),(unsigned char)(test)};
    // for(unsigned short int i = 0; i < sizeof(read_input_buf); i++){
    //     std::cout << std::hex << (int) read_input_buf[i] << std::dec << ' ';
    // }
    // std::cout << std::endl;
    //     bool enable=true;
    //     bool path=true;
    //     int value=0xcdab;
    //     unsigned char writeread_command_buf[] = {0x00,0x00, 0x02, 0x00,
    //     0x08, 0x00, 0x00, 0x00,
    //     0x30,0x00,0x00,0x02,
    //     (unsigned char)((unsigned char)((enable?1:0)<<7)|(unsigned char)((path?1:0)<<6)|((unsigned char)((value>>8)&0x0f))),
    //     (unsigned char)value,0xFF
    //     };
    //     for(unsigned short int i = 0; i < sizeof(writeread_command_buf); i++){
    //         std::cout << std::hex << (int) writeread_command_buf[i] << std::dec << ' ';
    // }
    //     std::cout << std::endl;
    //     std::cout <<"Set Load1: Enable? " <<enable<<" External "<<path<<" Value 0x"<<std::hex<<(value&0x0fff) << std::endl;
    // unsigned char read_input_buf[] = {0b11100101,0b11100101, 0b11100101, };
    // bool output;
    // //unsigned char temp=(read_input_buf[s/8]&(0x80>>(s%8))>>(7-(s%8)));
    // const char *stateName[16] =
    // {"P5V_overvoltage","P5V_undervoltage","P5V_overcurrent","P3V3_overvoltage","P3V3_undervoltage","P3V3_overcurrent","P2V5_overvoltage","P2V5_undervoltage","P2V5_overcurrent","P1V25_overvoltage","P1V25_undervoltage","P1V25_overcurrent","T1_error","T2_error","T3_error","T_SEH_error"};
    // for(unsigned short int s = 0; s < 16; s++){
    //     unsigned char temp=(read_input_buf[s/8]&(0x80>>(7-(s%8))))>>(s%8);
    //     std::cout<<stateName[s]<<": " <<std::hex<<(int)temp <<std::endl;
    // }
    //     std::cout << std::endl;
    // int temp=read_input_buf[m*2]<<(8) | read_input_buf[1+m*2];
    // std::cout <<  "0x"<<std::hex<<temp <<std::endl;
    // const char *supplyMeasurementName[10] ={"U_P5V", "I_P5V","U_P3V3", "I_P3V3","U_P2V5", "I_P2V5","U_P1V25", "I_P1V25","U_SEH", "I_SEH"};
    // std::cout<< supplyMeasurementName[m]<<": " <<"0x" << std::hex << (int) temp <<std::endl;

    // cTC_2SSEH.fuse(0x020,0xf8,0x24,0x44,0x55);
    // cTC_2SSEH.fuse(0x024,0x55,0x55,0x55,0x55);
    // cTC_2SSEH.fuse(0x028,0x0f,0x36,0x00,0x00);
    // cTC_2SSEH.fuse(0x02c,0xa0,0x89,0x99,0x0a);
    // cTC_2SSEH.fuse(0x030,0x1a,0x2a,0x3a,0x0a);
    // cTC_2SSEH.fuse(0x034,0x64,0x00,0x80,0x18);
    // cTC_2SSEH.fuse(0x038,0x00,0x40,0x20,0x03);

    // cTC_2SSEH.fuse(0x0ec,0x00,0x00,0x00,0x07);

    TC_2SSEH cTC_2SSEH;

    /* cTC_2SSEH.read_efuses(0x020);
    cTC_2SSEH.read_efuses(0x024);
    cTC_2SSEH.read_efuses(0x028);
    cTC_2SSEH.read_efuses(0x02c);
    cTC_2SSEH.read_efuses(0x030);
    cTC_2SSEH.read_efuses(0x034);
    cTC_2SSEH.read_efuses(0x038);  */

    // cTC_2SSEH.read_efuses(0x0ec);
    // cTC_2SSEH.read_reset(cTC_2SSEH.RST_CBC_L,k);
    // cTC_2SSEH.read_reset(cTC_2SSEH.RST_CIC_L,k);
    // cTC_2SSEH.read_reset(cTC_2SSEH.RST_CIC_R,k);
    // cTC_2SSEH.read_reset(cTC_2SSEH.RST_CBC_R,k);
    // cTC_2SSEH.write_and_check_i2c(0x53,0x49);
    // cTC_2SSEH.write_and_check_i2c(0x55,0x0);
    // cTC_2SSEH.read_i2c(0xef);
    // cTC_2SSEH.read_i2c(0x1c7);

    /* 	cTC_2SSEH.write_i2c(0x12c,0x00);
    cTC_2SSEH.write_i2c(0x12c,0x07);
    cTC_2SSEH.write_i2c(0x12c,0x00);
        std::this_thread::sleep_for (std::chrono::milliseconds (10) );
    cTC_2SSEH.read_i2c(0x15F);
    cTC_2SSEH.read_i2c(0x161);
    cTC_2SSEH.read_i2c(0x162);
    cTC_2SSEH.write_i2c(0x104,0xC);
        std::this_thread::sleep_for (std::chrono::milliseconds (10) );
    cTC_2SSEH.read_i2c(0x189);
    cTC_2SSEH.read_i2c(0x18B);
    cTC_2SSEH.read_i2c(0x18C);
    cTC_2SSEH.write_i2c(0x0FD,0x2);
        std::this_thread::sleep_for (std::chrono::milliseconds (10) );
    cTC_2SSEH.read_i2c(0x174);
    cTC_2SSEH.read_i2c(0x176);
    cTC_2SSEH.read_i2c(0x177);
    //cTC_2SSEH.write_i2c(0x0F7,0b01111000);
        for(int a=60; a<=60 ; a++){
    cTC_2SSEH.write_i2c(0x0F1,a);
    cTC_2SSEH.write_i2c(0x0FF,a);
    cTC_2SSEH.write_i2c(0x0F8,0x50);
    cTC_2SSEH.write_i2c(0x0F2,0b00001000);
    cTC_2SSEH.write_i2c(0x0F9,0b00000000);
    cTC_2SSEH.write_i2c(0x100,0b00001000);
    cTC_2SSEH.write_i2c(0x0F6,0x0);
    cTC_2SSEH.write_i2c(0x104,0x0);
    cTC_2SSEH.write_i2c(0x0FD,0x02);
    cTC_2SSEH.write_i2c(0x0F6,0x0);
    // cTC_2SSEH.write_i2c(0x0F2,0x0);
    // cTC_2SSEH.write_i2c(0x0F9,0x0);
    // cTC_2SSEH.write_i2c(0x100,0x0);
    // cTC_2SSEH.write_i2c(0x0F3,0x0);
    // cTC_2SSEH.write_i2c(0x0FA,0x0);
    // cTC_2SSEH.write_i2c(0x101,0x0);
    // cTC_2SSEH.write_i2c(0x0F6,0xC);
        std::this_thread::sleep_for (std::chrono::milliseconds (10) );
    cTC_2SSEH.read_i2c(0x15F);
    cTC_2SSEH.read_i2c(0x161);
    cTC_2SSEH.read_i2c(0x162);
    cTC_2SSEH.write_i2c(0x104,0xC);
        std::this_thread::sleep_for (std::chrono::milliseconds (10) );
    cTC_2SSEH.read_i2c(0x189);
    cTC_2SSEH.read_i2c(0x18B);
    cTC_2SSEH.read_i2c(0x18C);
    cTC_2SSEH.write_i2c(0x0FD,0x2);
        std::this_thread::sleep_for (std::chrono::milliseconds (10) );
    cTC_2SSEH.read_i2c(0x174);
    cTC_2SSEH.read_i2c(0x176);
    cTC_2SSEH.read_i2c(0x177);
    } */

    // cTC_2SSEH.write_i2c(0x020,0xf8);
    // cTC_2SSEH.write_i2c(0x021,0x24);
    // cTC_2SSEH.write_i2c(0x022,0x44);
    // cTC_2SSEH.write_i2c(0x023,0x55);

    // cTC_2SSEH.write_i2c(0x024,0x55);
    // cTC_2SSEH.write_i2c(0x025,0x55);
    // cTC_2SSEH.write_i2c(0x026,0x55);
    // cTC_2SSEH.write_i2c(0x027,0x55);

    // cTC_2SSEH.write_i2c(0x028,0x0f);
    // cTC_2SSEH.write_i2c(0x029,0x36);
    // cTC_2SSEH.write_i2c(0x02a,0x00);
    // cTC_2SSEH.write_i2c(0x02b,0x00);

    // cTC_2SSEH.write_i2c(0x02c,0xa0);
    // cTC_2SSEH.write_i2c(0x02d,0x89);
    // cTC_2SSEH.write_i2c(0x02e,0x99);
    // cTC_2SSEH.write_i2c(0x02f,0x0a);

    // cTC_2SSEH.write_i2c(0x030,0x1a);
    // cTC_2SSEH.write_i2c(0x031,0x2a);
    // cTC_2SSEH.write_i2c(0x032,0x3a);
    // cTC_2SSEH.write_i2c(0x033,0x0a);

    // cTC_2SSEH.write_i2c(0x034,0x64);
    // cTC_2SSEH.write_i2c(0x035,0x00);
    // cTC_2SSEH.write_i2c(0x036,0x80);
    // cTC_2SSEH.write_i2c(0x037,0x18);

    // cTC_2SSEH.write_i2c(0x038,0x00);
    // cTC_2SSEH.write_i2c(0x039,0x40);
    // cTC_2SSEH.write_i2c(0x03a,0x20);
    // cTC_2SSEH.write_i2c(0x03b,0x03);

    // cTC_2SSEH.write_and_check_i2c(0x0ef,0x06);
    // cTC_2SSEH.read_i2c(0x1c7);

    // cTC_2SSEH.writeI2C(0x00,0x53,0x20);
    // cTC_2SSEH.writeI2C(0x00,0x53,0x20);
    // cTC_2SSEH.readI2C(0x00,0x53,l);

    // cTC_2SSEH.sendLPGBTconfig(reg_value_buf,sizeof(reg_value_buf));
    //-------------------------------------------
    //     while(true){
    //         cTC_2SSEH.read_hvmon(cTC_2SSEH.VHVJ8,k);
    //         std::this_thread::sleep_for (std::chrono::milliseconds (200) );
    //     }
    //     cTC_2SSEH.read_temperature(cTC_2SSEH.Temp1,k);
    //     cTC_2SSEH.read_temperature(cTC_2SSEH.Temp2,k);
    //     cTC_2SSEH.read_temperature(cTC_2SSEH.Temp3,k);
    //     cTC_2SSEH.read_temperature(cTC_2SSEH.Temp_SEH,k);

    //     cTC_2SSEH.read_supply(cTC_2SSEH.U_P5V,k);
    //     cTC_2SSEH.read_supply(cTC_2SSEH.I_P5V,k);
    //     cTC_2SSEH.read_supply(cTC_2SSEH.U_P3V3,k);
    //     cTC_2SSEH.read_supply(cTC_2SSEH.I_P3V3,k);
    //     cTC_2SSEH.read_supply(cTC_2SSEH.U_P2V5,k);
    //     cTC_2SSEH.read_supply(cTC_2SSEH.I_P2V5,k);
    //     cTC_2SSEH.read_supply(cTC_2SSEH.U_P1V25,k);
    //     cTC_2SSEH.read_supply(cTC_2SSEH.I_P1V25,k);
    // for(int trys=0;trys<100;trys++)
    // {cTC_2SSEH.read_temperature(cTC_2SSEH.Temp1,k);
    // cTC_2SSEH.set_load2(false, false, 0);}
    // cTC_2SSEH.read_temperature(cTC_2SSEH.Temp1,k);
    cTC_2SSEH.set_HV(true, false, false, 0);
    while(true)
    {
        cTC_2SSEH.read_hvmon(cTC_2SSEH.HV_meas, k);
        std::this_thread::sleep_for(std::chrono::milliseconds(1200));
    }
    // cTC_2SSEH.set_SehSupply(cTC_2SSEH.sehSupply_On);
    // //std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    // cTC_2SSEH.set_load1(true,false, 1147); //1260 1674
    // cTC_2SSEH.set_load2(true,false, 1210); //1330 1764
    // //std::this_thread::sleep_for(std::chrono::milliseconds(3000));
    // cTC_2SSEH.read_supply(cTC_2SSEH.U_SEH, k);
    // cTC_2SSEH.read_load(cTC_2SSEH.U_P1V2_R, k);

    // cTC_2SSEH.read_load(cTC_2SSEH.U_P1V2_R, k);
    // cTC_2SSEH.read_load(cTC_2SSEH.I_P1V2_R, k);
    // cTC_2SSEH.read_load(cTC_2SSEH.U_P1V2_L, k);
    // cTC_2SSEH.read_load(cTC_2SSEH.I_P1V2_L, k);
    //     cTC_2SSEH.read_load(cTC_2SSEH.P2V5_VTRx_MON,k);

    // cTC_2SSEH.read_hvmon(cTC_2SSEH.Mon,k);
    // cTC_2SSEH.set_HV(true,false,false,0);//0x155=100V
    //     cTC_2SSEH.read_temperature(cTC_2SSEH.Temp1,k);
    // cTC_2SSEH.read_hvmon(cTC_2SSEH.HV_meas, k);
    // cTC_2SSEH.set_HV(true, false, false, 0x155); // 0x155=100V
    //     cTC_2SSEH.read_temperature(cTC_2SSEH.Temp1,k);
    //     cTC_2SSEH.read_hvmon(cTC_2SSEH.VHVJ7,k);
    //     cTC_2SSEH.read_temperature(cTC_2SSEH.Temp1,k);
    //     cTC_2SSEH.read_hvmon(cTC_2SSEH.VHVJ8,k);

    //     cTC_2SSEH.read_state(cTC_2SSEH.P5V_overvoltage,wahr);

    //     cTC_2SSEH.read_state(cTC_2SSEH.P5V_undervoltage,wahr);
    //     cTC_2SSEH.read_reset(cTC_2SSEH.RST_CBC_L,k);
    //     cTC_2SSEH.read_reset(cTC_2SSEH.RST_CBC_R,k);
    //     cTC_2SSEH.read_reset(cTC_2SSEH.RST_CIC_L,k);
    //     cTC_2SSEH.read_reset(cTC_2SSEH.RST_CIC_R,k);

    //     cTC_2SSEH.set_HV(false,false,true,0x155);//0x155=100V
    // cTC_2SSEH.set_SehSupply(cTC_2SSEH.sehSupply_On);
    //     cTC_2SSEH.set_fuse(false);
    // for(unsigned short int i = 0x20; i < 0x3c; i++){
    //     cTC_2SSEH.read_i2c(i);
    // }

    // cTC_2SSEH.read_i2c(0xef);
    // cTC_2SSEH.read_i2c(0x140);

    // cTC_2SSEH.set_AMUX(0xe00,0xe000);
    // cTC_2SSEH.read_reset(cTC_2SSEH.RST_CBC_L,k);
    // cTC_2SSEH.toggle_led();
    // cTC_2SSEH.set_SehSupply(cTC_2SSEH.sehSupply_On);
    // cTC_2SSEH.set_load1(false,false, 0x9c4);
    // cTC_2SSEH.set_load1(false,false, 1634);
    // cTC_2SSEH.set_load2(false,false, 1634);
    // cTC_2SSEH.set_HV(true,true,true,0x55);
    // cTC_2SSEH.read_state(cTC_2SSEH.T_SEH_error,wahr);
    // cTC_2SSEH.set_limit(cTC_2SSEH.I_P1V25_max,0x34);
    // cTC_2SSEH.read_limit(cTC_2SSEH.I_P1V25_max,k);
    // cTC_2SSEH.set_fuse(false);
    // cTC_2SSEH.read_i2c(0x1c7);
    // cTC_2SSEH.write_and_check_i2c(0x12c,0x0);
    // cTC_2SSEH.write_and_check_i2c(0x12c,0x7);
    // cTC_2SSEH.write_and_check_i2c(0x12c,0x0);
    // while(true)
    // {
    //     cTC_2SSEH.write_and_check_i2c(0x0F2, 0x3);
    //     cTC_2SSEH.write_i2c(0x0F6, 0x0);
    //     cTC_2SSEH.write_i2c(0x0F1, 0x60);
    //     cTC_2SSEH.write_i2c(0x0F2, 0x9);
    //     cTC_2SSEH.write_i2c(0x0F6, 0x2);
    //     cTC_2SSEH.read_i2c(0x161);
    // }
    // cTC_2SSEH.write_i2c(0x100, 0x3);
    // cTC_2SSEH.write_i2c(0x104, 0x0);
    // cTC_2SSEH.write_i2c(0x0FF, 0x60);
    // cTC_2SSEH.write_i2c(0x100, 0x9);
    // cTC_2SSEH.write_i2c(0x104, 0x2);
    // cTC_2SSEH.read_i2c(0x18b);
    // a test
    return 0;
}
